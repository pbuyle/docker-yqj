FROM mikefarah/yq as yq
FROM sclevine/yj as yj
FROM alpine
COPY --from=yq /usr/bin/yq /usr/bin/yq
COPY --from=yj /bin/yj /usr/bin/yj